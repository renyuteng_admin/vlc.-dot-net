﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Vlc.DotNet.Core;

namespace WinFormsDemo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            var currentDirectory = @"D:\VedioDemo\NewAPlayerDemo\VLCPlayer\bin\Debug\libvlc\win-x64";
            var vlcLibDirectory = new DirectoryInfo(currentDirectory);
            vlcControl1.VlcLibDirectory = vlcLibDirectory;
            ((System.ComponentModel.ISupportInitialize)(this.vlcControl1)).EndInit();

            string path = @"C:\Users\yuteng\Videos\u11.mp4";
            vlcControl1.SetMedia(new System.IO.FileInfo(path));
            vlcControl1.Play();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //IEnumerable<AudioOutputDescription> o = vlcControl1.Audio.Outputs.All;
            //foreach (var item in o)
            //{
            //    //Console.WriteLine($"Description:{item.Description},Devices:{item.Devices},Name:{item.Name},");

            //    foreach (var devItem in item.Devices)
            //    {
            //        Console.WriteLine($"{devItem.Description}");
            //    }
            //}

            vlcControl1.SetAudioOutput("waveout");
            vlcControl1.SetAudioOutputDevice("waveout", "扬声器 (Realtek High Definition Audio)");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            vlcControl1.SetAudioOutput("waveout");
            vlcControl1.SetAudioOutputDevice("waveout", "默认");
        }
    }
}
