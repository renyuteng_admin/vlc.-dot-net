# Vlc.DotNet

#### 介绍
Vlc.DotNet的源码，C#调用VLC的API播放视频
增加了播放器控件设置声卡的功能，其他与github上的Vlc.DotNet同步。

Vlc.DotNet
As more and more effort is put into LibVLCSharp, fewer evolutions are expected to be made to this project.

Bugfixes will still be fixed on the maintainer's free time, but as of 2020-05-06, support for libvlc 4 is not planned. You are encouraged to migrate and create new projects with LibVLCSharp

Join the chat at https://discord.gg/SyUpk57

Vlc.DotNet is a .net library that hosts the audio/video capabilities of the VLC libraries. In other words, it's a .net wrapper around libvlc.

It can work on any .net framework version starting from .net 2.0 and .net standard 1.3 (starting from Vlc.DotNet 2.2.1).

On the front-end side, two components are currently available to make it easy to integrate in your apps. One is for WinForms, the other for WPF.

Writing a WPF app / Migrating WPF control from 2.x
Please Read this if you are writing a WPF app! This is super important!

tl;dr : Use Vlc.DotNet.Forms in a WindowsFormsHost, unless you know what you're doing

The WPF control has been rewritten from scratch from 2.x.

The old WPF control was just a wrapper around the WinForms control. This led to some issues (Airspace issue...) and lacked some WPF-ish features.

That's why a new control has been written. To be fair, first versions of Vlc.DotNet were built with that technique, but back then, there were issues in the .net framework causing the memory usage to explode. As of 2018, this issue is resolved.

You have in fact two options:

Use the new WPF control. You might notice a performance impact when reading, for example, a 4k @ 60 fps video on a low-end computer. However, you can do whatever you like, just as a normal ImageSource in WPF.
Wrap the Vlc.DotNet.WinForms control in a WindowsFormsHost. It offers better performance, but you will experience Airspace issues (see #296) if you need to write over the video.
The right option to use depends on your needs.

See the discussion #249 and pull request : #365

How to use
It all starts with those three steps :

Install one of the NuGet Packages below
Install libvlc libraries manually or from the NuGet package(recommended)
Integrate it into your app
