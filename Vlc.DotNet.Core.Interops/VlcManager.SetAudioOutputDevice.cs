﻿using Vlc.DotNet.Core.Interops.Signatures;

namespace Vlc.DotNet.Core.Interops
{
    public sealed partial class VlcManager
    {
        public void SetAudioOutputDevice(VlcMediaPlayerInstance mediaPlayerInstance, string audioOutputDescriptionName, string deviceName)
        {
            using (var audioOutputInterop = Utf8InteropStringConverter.ToUtf8StringHandle(audioOutputDescriptionName))
            using (var deviceNameInterop = Utf8InteropStringConverter.ToUtf8StringHandle(deviceName))
            {
                myLibraryLoader.GetInteropDelegate<SetAudioOutputDevice>().Invoke(mediaPlayerInstance, audioOutputInterop, deviceNameInterop);      //扬声器：0x0000012bb614dfb0，0x0000012bb6019290
                //myLibraryLoader.GetInteropDelegate<SetAudioOutputDevice>().Invoke(mediaPlayerInstance, audioOutputDescriptionName, deviceNameInterop);
            }
        }
    }
}
